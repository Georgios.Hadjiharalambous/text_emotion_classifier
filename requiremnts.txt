Install following python packages with pip:

transformers 
torch==1.6.0 
keras 

and then download the weights and python file:
git clone https://gitlab.com/Georgios.Hadjiharalambous/text_emotion_classifier.git

and run :
cd text_emotion_classifier
python text_emotion_predictor.py
