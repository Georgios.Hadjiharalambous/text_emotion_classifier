# text_emotion_classifier

This repository has a notebook for a text emotion classifier models using Bert and its weights.

The link for the notebook is [this](https://colab.research.google.com/drive/1Z13MnF93i4kxchEL7-V-efJNuPf5JG9N?usp=sharing).

You can copy the notebook and run live with your own sentences.

Link to the original project that has comparison with other similar models and variations and the training and evaluation phases can be found [here](https://gitlab.com/Georgios.Hadjiharalambous/emotion-detection-in-short-texts-datasets-with-bert).

Docker implementation of training and testing can be found [here](https://gitlab.com/Georgios.Hadjiharalambous/text-emotion-detection-using-bert-docker)

Datasets used can be found in this [link](https://gitlab.com/Georgios.Hadjiharalambous/emotion-detection-in-short-texts-datasets)